import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class LongStack {

    public static void main(String[] argum) {
        LongStack m = new LongStack();
        m.pop();
//      m.push (-8);
//      m.push (0);
//      m.op("/");
//      m.push (7);
//      m.push (2);
//      System.out.println(m.toString());

//      System.out.println(Long.parseLong("-3"));
        String s = "3 4 + - ";
        LongStack m2 = new LongStack();
        long res = interpret(s);
        System.out.println(res);

    }

    private LinkedList<Long> stack = new LinkedList<>();
    private static List<String> mAperaters = Arrays.asList("+", "-", "*", "/");

    LongStack() {
    }

    LongStack(LinkedList l) {
        stack = new LinkedList<>(l);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return new LongStack(this.stack);
    }

    public boolean stEmpty() {
        return this.stack.size() == 0;
    }

    public void push(long a) {
        stack.add(a);
    }

    public long pop() {
        if (stack.isEmpty()) {
            throw new RuntimeException("Not enough elements");
        }

        return this.stack.removeLast();
    }

    public void op(String s) {

        if (!mAperaters.contains(s)) {
            throw new RuntimeException("'" + s + "'" + "- " + "Invalid math operator");
        }

        if (size() < 2) {
            throw new RuntimeException("Not enough element for operation " + s);
        }

        long b = pop();
        long a = pop();
        long res = 0;
        if (s.equals("-")) {
            res = a - b;
        }
        if (s.equals("+")) {
            res = a + b;
        }
        if (s.equals("/")) {
            if (b == 0) {
                throw new RuntimeException("Division by zero error: " + a + s + b);
            }
            res = a / b;
        }
        if (s.equals("*")) {
            res = a * b;
        }
        push(res);
    }

    public long tos() {
        if (stack.isEmpty()) {
            throw new RuntimeException("Not enough elements");
        }
        return this.stack.getLast();
    }

    public int size() {
        return this.stack.size();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof LongStack)) {
            return false;
        }
        LongStack oStack = (LongStack) o;

        if (size() != oStack.size()) {
            return false;
        }


        for (int i = 0; i < size(); i++) {
            // memo: https://howtodoinjava.com/java/oops/instances-of-same-class-can-access-private-members-of-each-other/
            if (!this.stack.get(i).equals(oStack.stack.get(i))) {
                return false;
            }
        }

        return true;
    }

    @Override
    public String toString() {
        StringBuilder stringStack = new StringBuilder();

        for (int i = 0; i < this.stack.size(); i++) {
            if (i > 0) {
                stringStack.append(" ");
            }
            stringStack.append(this.stack.get(i));
        }

        return stringStack.toString();
    }

    public static long interpret(String pol) {
        String formattedPol = pol.replaceAll("\t|\r|\n|\b", "");
        formattedPol = formattedPol.trim().replaceAll("\\s+", " ");


        char[] chArr = formattedPol.toCharArray();

        if (chArr.length < 1) {
            throw new RuntimeException("Empty expression");
        }

        LongStack stack = new LongStack();
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < chArr.length; i++) {
            char c = chArr[i];

            if (c == ' ') {
                if (stringBuilder.length() != 0) {
                    String numS = stringBuilder.toString();

                    try {
                        Long numL = Long.parseLong(numS);
                        stack.push(numL);
                        stringBuilder = new StringBuilder();
                    } catch (Exception e) {
                        throw new RuntimeException("Invalid characters in '" + pol + "' expression");
                    }

                }
                continue;
            }

            if (c == '-' && i < chArr.length - 1 && chArr[i + 1] != ' ') {
                stringBuilder.append(c);
                continue;
            }

            if (mAperaters.contains(String.valueOf(c))) {
                try {
                    stack.op(String.valueOf(c));
                    continue;
                } catch (Exception e) {
                    throw new RuntimeException("Invalid expression '" + pol + "' reason:  " + e.getMessage());
                }

            }

            stringBuilder.append(c);

            if (i == chArr.length - 1) {
                String numS = stringBuilder.toString();
                Long numL = Long.parseLong(numS);
                stack.push(numL);
            }
        }

        if (stack.size() > 1) {
            throw new RuntimeException("Too many numbers in '" + pol + "' expression");
        }

        return stack.tos();
    }

}

